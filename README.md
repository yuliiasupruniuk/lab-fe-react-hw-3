# LAB FE React HW 2

This is a creating couses app. useReducer played a major role. 


## How to run?
 Please make sure you have installed node and npm in your system.
 ```
 node -v
 npm -v
 ```
 
 After checking if you have Node installed in your system, you can start app:
 ```
 git clone https://gitlab.com/yuliiasupruniuk/lab-fe-react-hw-2.git
 cd lab-fe-react-hw-2
 npm install
 npm start
