import Button from '../common/Button/Button';
import Logo from './components/Logo/Logo';
import { logout } from '../../services/authService';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { removeUser } from '../../store/user/actionCreators';

function Header() {
	const navigate = useNavigate();
	const username = useSelector((state) => state.user.name);
	const dispatch = useDispatch();

	const logoutUser = () => {
		logout();
		dispatch(removeUser());
		navigate('/login');
	};

	return (
		<nav className='navbar navbar-light alert-primary text-dark'>
			<a className='navbar-brand' href='/'>
				<Logo />
			</a>

			<div className='d-flex align-items-end'>
				<h6 className='mr-3'>{username}</h6>
				<Button buttonText='Logout' color='warning' onClick={logoutUser} />
			</div>
		</nav>
	);
}
export default Header;
