import { Link, useParams } from 'react-router-dom';
import { dateGenerator } from '../../helpers/dateGeneratop';
import durationInHours from '../../helpers/pipeDuration';

function CourseInfo(props) {
	const { id } = useParams();

	const course = props.state.coursesList.filter(
		(course) => course.id === id
	)[0];

	const creationDate = dateGenerator(course.creationDate);
	const duration = durationInHours(course.duration);
	const courseAuthors = props.state.authorsList.filter(
		(author) => course.authors.indexOf(author.id) !== -1
	);
	const authorsList = courseAuthors.map((author) => {
		return <li>{author.name}</li>;
	});

	return (
		<div className='border border-primary container-fluid p-4 mt-4 my-3'>
			<Link className='text-dark' to='/courses'>
				&#60; Back to courses
			</Link>
			<div className='my-2'>
				<h2 className='text-center'>{course.title}</h2>
				<div className='row my-5'>
					<p className='col-9 pr-3'>{course.description}</p>
					<div className=''>
						<p>
							<strong>ID: </strong>
							{course.id}
						</p>
						<p>
							<strong>Duration: </strong>
							{duration}
						</p>
						<p>
							<strong>Created: </strong>
							{creationDate}
						</p>
						<p>
							<strong>Authors:</strong>
							<ul className='list-unstyled'>{authorsList}</ul>
						</p>
					</div>
				</div>
			</div>
		</div>
	);
}

export default CourseInfo;
