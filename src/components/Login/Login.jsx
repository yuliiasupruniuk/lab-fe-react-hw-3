import { Link } from 'react-router-dom';
import { login } from '../../services/authService';
import AuthForm from '../common/AuthForm/AuthForm';

function Login() {
	const navigateLink = '/courses';
	const action = 'Login';
	const actionLink = (
		<p className='text-center mt-3'>
			If you not have an account you can{' '}
			<Link to='/registration'>Registration</Link>
		</p>
	);

	return (
		<AuthForm
			navigateLink={navigateLink}
			action={action}
			actionLink={actionLink}
			authMethod={login}
		/>
	);
}

export default Login;
