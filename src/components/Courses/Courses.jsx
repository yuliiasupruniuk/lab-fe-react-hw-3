import { useNavigate } from 'react-router-dom';
import Button from '../common/Button/Button';
import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';

function Courses(props) {
	const state = props.state;
	const dispatch = props.dispatch;
	const navigate = useNavigate();

	const findAuthors = (authorsIds) => {
		const authors = state.authorsList.filter(
			(author) => authorsIds.indexOf(author.id) !== -1
		);
		return authors;
	};

	const coursesList = state.coursesList.map((course) => {
		const authors = findAuthors(course.authors);
		return <CourseCard key={course.id} course={course} authors={authors} />;
	});

	const showCreationForm = () => {
		navigate('add');
	};

	const content = (
		<div>
			<div className='d-flex justify-content-between my-3'>
				<SearchBar state={state} dispatch={dispatch} />
				<Button buttonText='Add new course' onClick={showCreationForm} />
			</div>
			<ul className='list-group'>{coursesList}</ul>
		</div>
	);

	return content;
}

export default Courses;
