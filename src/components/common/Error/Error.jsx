function Error(props) {
	return (
		<div class='alert alert-danger col-auto' role='alert'>
			{props.message}
		</div>
	);
}
export default Error;
