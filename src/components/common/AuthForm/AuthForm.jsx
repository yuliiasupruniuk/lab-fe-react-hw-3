import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import validateEmail from '../../../helpers/validateEmail';
import { setUser } from '../../../store/user/actionCreators';
import Button from '../Button/Button';
import Error from '../Error/Error';
import Input from '../Input/Input';

function AuthForm(props) {
	const dispatch = useDispatch();
	const authMethod = props.authMethod;
	const navigate = useNavigate();
	const [authForn, setField] = useState({
		name: {
			touched: false,
			valid: false,
			value: '',
			errorMessage: `Field can't be empty`,
		},
		email: {
			touched: false,
			valid: false,
			value: '',
			errorMessage: 'Invalid email',
		},
		password: {
			touched: false,
			valid: false,
			value: '',
			errorMessage: `Length should be minimum 6 symbols`,
		},
	});
	const [error, setError] = useState('');

	const setValue = (field, value, valid = true) => {
		let isValid = !!value.trim() && valid;
		setField((prev) => {
			return {
				...prev,
				[field]: {
					...prev[field],
					value: value,
					touched: true,
					valid: isValid,
				},
			};
		});
	};

	const isEmailValid = (email) => {
		return validateEmail(email);
	};

	const setEmail = (value) => {
		const isValid = isEmailValid(value);
		setValue('email', value, isValid);
	};

	const setPassword = (value) => {
		const isValid = value.length >= 6;
		setValue('password', value, isValid);
	};

	const isFieldInvalid = (field) => {
		const errorMsg = authForn[field].errorMessage;
		const errorAllert = (
			<small className='text-danger col-auto'>{errorMsg}</small>
		);

		if (authForn[field].touched && !authForn[field].valid) {
			return errorAllert;
		}
	};

	const handleError = (response) => {
		if (response?.error) {
			setError(response?.errors[0]);
		} else if (response?.result) {
			setError(response?.result);
		}
		setTimeout(() => setError(''), 3000);
	};

	const saveUser = (response) => {
		const user = {
			name: response.user.name,
			email: response.user.email,
			token: response.result,
		};

		dispatch(setUser(user));
	};

	const handleResponse = (response) => {
		if (response.successful) {
			if (response?.user) {
				saveUser(response);
			}
			navigate(props.navigateLink);
		} else {
			handleError(response);
		}
	};

	const isFormValid = () => {
		const course = Object.entries(authForn);
		return !course.filter((field) => !field[1].touched || !field[1].valid)
			.length;
	};

	const auth = async () => {
		if (isFormValid()) {
			const userInfo = {
				email: authForn.email.value,
				password: authForn.password.value,
				name: authForn.name.value,
			};
			const response = await authMethod(userInfo);
			handleResponse(response);
		} else {
			alert('Validation failed');
		}
	};

	const submit = (event) => {
		event.preventDefault();
		auth();
	};

	const serverError = (errorMessage) => {
		return <Error message={errorMessage} />;
	};

	return (
		<div className='center auth-form'>
			<form onSubmit={submit}>
				<h3 className='text-center mb-3'>{props.action}</h3>
				{error && serverError(error)}
				<div className='form-group'>
					<Input
						labelText='Name'
						placeholder='Enter name'
						onChange={(value) => setValue('name', value)}
					/>
					{isFieldInvalid('name')}
				</div>
				<div className='form-group'>
					<Input
						labelText='Email'
						placeholder='Enter email'
						onChange={(value) => setEmail(value)}
					/>
					{isFieldInvalid('email')}
				</div>
				<div className='form-group'>
					<Input
						type='password'
						labelText='Password'
						placeholder='Enter password'
						onChange={(value) => setPassword(value)}
					/>
					{isFieldInvalid('password')}
				</div>

				<div className='d-flex justify-content-center'>
					<Button type='submit' buttonText={props.action} onClick={() => {}} />
				</div>

				{props.actionLink}
			</form>
		</div>
	);
}
export default AuthForm;
