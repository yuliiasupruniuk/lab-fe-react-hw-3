function Button(props) {
	const styleClasses = [
		'btn',
		'font-weight-bold',
		'border',
		'border-primary',
		`btn-${props.color}`,
	];
	const type = props.type || 'button';

	return (
		<div>
			<button
				type={type}
				className={styleClasses.join(' ')}
				onClick={() => props.onClick()}
			>
				{props.buttonText}
			</button>
		</div>
	);
}
export default Button;
