import { Link } from 'react-router-dom';
import { register } from '../../services/authService';
import AuthForm from '../common/AuthForm/AuthForm';

function Registration() {
	const navigateLink = '/login';
	const action = 'Registration';
	const actionLink = (
		<p className='text-center mt-3'>
			If you have an account you can <Link to='/login'>Login</Link>
		</p>
	);

	return (
		<AuthForm
			navigateLink={navigateLink}
			action={action}
			actionLink={actionLink}
			authMethod={register}
		/>
	);
}

export default Registration;
