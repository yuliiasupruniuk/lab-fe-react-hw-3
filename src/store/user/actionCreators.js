import { REMOVE_USER, SET_USER } from './actionTypes';

function setUser(user) {
	const { name, email, token } = user;
	return {
		type: SET_USER,
		payload: {
			name,
			email,
			token,
		},
	};
}

function removeUser(text) {
	return { type: REMOVE_USER, payload: text };
}

export { setUser, removeUser };
