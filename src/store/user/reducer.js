import { getToken, getUserInfo } from '../../services/authService';
import { REMOVE_USER, SET_USER } from './actionTypes';

function getUser() {
	const user = getUserInfo();
	user.token = getToken();
	user.isAuth = !!user?.token;
	return user;
}

const userInitialState = {
	isAuth: getUser().isAuth,
	name: getUser().name || '',
	email: getUser().email || '',
	token: getUser().token || '',
};

export default function userReducer(state = userInitialState, action) {
	switch (action.type) {
		case SET_USER:
			const { name, email, token } = action.payload;
			return {
				isAuth: true,
				name,
				email,
				token,
			};
		case REMOVE_USER:
			return {
				isAuth: false,
				name: '',
				email: '',
				token: '',
			};
		default:
			break;
	}
	return state;
}
