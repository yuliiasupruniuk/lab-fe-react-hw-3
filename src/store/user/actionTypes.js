const SET_USER = 'user/save-current-user';
const REMOVE_USER = 'user/remove-current-user';

export { SET_USER, REMOVE_USER };
