function searchPipe(str, courses) {
	if (!str?.trim()) {
		return courses;
	}
	return courses.filter(
		(course) =>
			course['id'].toString().toLowerCase().includes(str.toLowerCase()) ||
			course['title'].toString().toLowerCase().includes(str.toLowerCase())
	);
}

export default searchPipe;
