function durationInHours(minutes) {
	let hours = Math.floor(minutes / 60);
	let min = minutes % 60;
	hours = hours < 10 ? '0' + hours : hours;
	min = min < 10 ? '0' + min : min;
	return hours + ':' + min;
}

export default durationInHours;
