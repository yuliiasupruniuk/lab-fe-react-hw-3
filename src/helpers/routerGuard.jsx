import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import Header from '../components/Header/Header';
import { isAuthenticated } from '../services/authService';

const GuardedRoute = ({ component: Component, ...rest }) => {
	return isAuthenticated() ? (
		<>
			<Header />
			<Outlet />
		</>
	) : (
		<Navigate to='/login' />
	);
};

export default GuardedRoute;
