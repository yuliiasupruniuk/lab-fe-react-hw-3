import React, { useReducer } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import './App.css';
import CourseInfo from './components/CourseInfo/CourseInfo';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';
import Login from './components/Login/Login';
import Registration from './components/Registration/Registration';
import { initialState, reducer } from './helpers/reducer';
import GuardedRoute from './helpers/routerGuard';

function App() {
	const [state, dispatch] = useReducer(reducer, initialState);

	return (
		<div className='wrapper text-dark'>
			<Routes>
				<Route path='/' element={<Navigate to='/courses' />} />
				<Route exact path='/' element={<GuardedRoute />}>
					<Route
						exact
						path={('/', '/courses')}
						element={<Courses state={state} dispatch={dispatch} />}
					/>
					<Route
						exact
						path='/courses/add'
						element={<CreateCourse state={state} dispatch={dispatch} />}
					/>
					<Route
						exact
						path='/courses/:id'
						element={<CourseInfo state={state} />}
					/>
				</Route>
				<Route exact path='/registration' element={<Registration />} />
				<Route exact path='/login' element={<Login />} />
			</Routes>
		</div>
	);
}
export default App;
